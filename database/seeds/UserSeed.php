<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Str;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'last_name'=>'Администратор',
            'password' => bcrypt('password'),
            'api_token' => Str::random(60),
        ]);
        $user->assignRole('administrator');

    }
}
