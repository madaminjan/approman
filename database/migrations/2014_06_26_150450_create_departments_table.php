<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->enum('type',['DIV','DEP']);
            $table->string('name_uz')->index();
            $table->string('name_ru')->index();
            $table->string('code',25)->nullable();
            $table->unsignedInteger('head_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['parent_id','name_uz']);
            $table->unique(['parent_id','name_ru']);

            $table->foreign('parent_id')->references('id')->on('departments')
                ->onDelete('set null')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
