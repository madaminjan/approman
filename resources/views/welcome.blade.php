@extends('layouts.app')

@section('content')
    <!-- Content area -->
    <div class="ui container content">
        <div class="ui stackable doubling grid">

            <!-- Middle Content -->
            <div class="twelve wide column">
                <div align="center">
                    <div class="ui divider"></div>
                    <div class="ui items">
                        <div class="item" align="left">

                            <div class="image">
                                <img style="width: 100px;" src="{{asset('img/filetypes/docx.png')}}">
                            </div>
                            <div class="content">
                                {{--<a class="ui red ribbon label">Новое</a>--}}
                                <a class="header">Проект контракта с компанией N</a>
                                <div class="meta">
                                    <span>
                                        <i class="calendar icon"></i> 10/05/2019
                                        <i class="user icon"></i> Эшматов Тошмат
                                         <i class="handshake icon"></i> 23
                                         <i class="comments icon"></i> 5
                                    </span>

                                </div>
                                <div class="description" align="justify">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi vel, itaque ipsum iste ex. Quam reiciendis voluptatem reprehenderit natus aliquam dolore laudantium eaque praesentium ipsum itaque repellendus, nulla at error.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, nihil consequatur! Vero illum illo, nesciunt, tempora accusamus praesentium sunt...</p>
                                </div>
                                <div class="extra">
                                    <div class="ui grey right floated small button">Подробнее</div>
                                </div>
                            </div>
                        </div>
                        <div class="ui divider"></div>
                        <div class="item" align="left">
                            <div class="image">
                                <img style="width: 100px;" src="{{asset('img/filetypes/pdf.png')}}">
                            </div>
                            <div class="content">
                                {{--<a class="ui teal ribbon label">Согласовано</a>--}}
                                <a class="header">Список участников конференции</a>
                                <div class="meta">
                                    <span>
                                        <i class="calendar icon"></i> 10/05/2019
                                        <i class="user icon"></i> Алиев Вали
                                         <i class="share icon"></i> 8
                                         <i class="comments icon"></i> 2
                                    </span>
                                </div>
                                <div class="description" align="justify">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi vel, itaque ipsum iste ex. Quam reiciendis voluptatem reprehenderit natus aliquam dolore laudantium eaque praesentium ipsum itaque repellendus, nulla at error.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, nihil consequatur! Vero illum illo, nesciunt, tempora accusamus praesentium sunt...</p>
                                </div>
                                <div class="extra">
                                    <div class="ui blue right floated small button">Подробнее</div>
                                </div>
                            </div>
                        </div>
                        <div class="ui divider"></div>
                        <div class="item" align="left">
                            <div class="image">
                                <img style="width: 100px;" src="{{asset('img/filetypes/xlsx.png')}}">
                            </div>
                            <div class="content">
                                {{--<a class="ui orange ribbon label">На доработке</a>--}}
                                <a class="header">Learn About Angular 2 - Part 2</a>
                                <div class="meta">
                                    <span><i class="calendar icon"></i> 10/04/2016 <i class="tag icon"></i> Javascript <i class="user icon"></i> Muhibbudin Suretno</span>
                                </div>
                                <div class="description" align="justify">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi vel, itaque ipsum iste ex. Quam reiciendis voluptatem reprehenderit natus aliquam dolore laudantium eaque praesentium ipsum itaque repellendus, nulla at error.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, nihil consequatur! Vero illum illo, nesciunt, tempora accusamus praesentium sunt...</p>
                                </div>
                                <div class="extra">
                                    <div class="ui blue right floated small button">Подробнее</div>
                                </div>
                            </div>
                        </div>
                        <div class="ui divider"></div>
                        <div class="item" align="left">
                            <div class="image">
                                <img src="assets/img/400x400.png">
                            </div>
                            <div class="content">
                                <a class="header">How to make Chat App</a>
                                <div class="meta">
                                    <span><i class="calendar icon"></i> 10/04/2016 <i class="tag icon"></i> Javascript <i class="user icon"></i> Muhibbudin Suretno</span>
                                </div>
                                <div class="description" align="justify">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi vel, itaque ipsum iste ex. Quam reiciendis voluptatem reprehenderit natus aliquam dolore laudantium eaque praesentium ipsum itaque repellendus, nulla at error.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, nihil consequatur! Vero illum illo, nesciunt, tempora accusamus praesentium sunt...</p>
                                </div>
                                <div class="extra">
                                    <div class="ui blue right floated small button">Подробнее</div>
                                </div>
                            </div>
                        </div>
                        <div class="ui divider"></div>
                        <div class="item" align="left">
                            <div class="image">
                                <img src="assets/img/400x400.png">
                            </div>
                            <div class="content">
                                <a class="header">Learn About Angular 2 - Part 3</a>
                                <div class="meta">
                                    <span><i class="calendar icon"></i> 10/04/2016 <i class="tag icon"></i> Javascript <i class="user icon"></i> Muhibbudin Suretno</span>
                                </div>
                                <div class="description" align="justify">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi vel, itaque ipsum iste ex. Quam reiciendis voluptatem reprehenderit natus aliquam dolore laudantium eaque praesentium ipsum itaque repellendus, nulla at error.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis, nihil consequatur! Vero illum illo, nesciunt, tempora accusamus praesentium sunt...</p>
                                </div>
                                <div class="extra">
                                    <div class="ui blue right floated small button">Подробнее</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ui divider"></div>
                    <div class="ui buttons">
                        <button class="ui button disabled">Previous</button>
                        <button class="ui blue button">1</button>
                        <button class="ui button">2</button>
                        <button class="ui button">3</button>
                        <button class="ui button">...</button>
                        <button class="ui button">n</button>
                        <button class="ui button">Next</button>
                    </div>
                </div>
            </div>
            <!-- Left Sidebar -->
            <div class="four wide column">
                <div class="ui divider"></div>
                <div class="ui fluid vertical menu moderns">
                    <a href="" class="header item">категории</a>
                    <a href="" class="item"><div class="ui small orange label">51</div>Мне на утверждение</a>
                    <a href="" class="item"><div class="ui small blue label">14</div>Создано мной</a>
                </div>
                <button class="ui basic teal right floated button">Создать</button>
            </div>
        </div>
    </div>




@endsection