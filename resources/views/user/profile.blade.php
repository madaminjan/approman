@extends('layouts.profile')

@section('content')
    <!-- Content area -->
   <div class="ui centered grid">
        <div class="row">
            <div class="ten wide column">
                <div class="ui top attached segment">
                    <h4>User profile</h4>
                </div>
                <div class="ui attached segment">
                    <div class="ui items" id="profile-row">
                        <div class="item">
                            <a class="ui small circular image">
                                <img src="{{route('avatar.get')}}" alt="avatar" />
                            </a>
                            <div class="middle aligned content hiddenui">
                                <h2 class="ui header">
                                    <div class="content">
                                        {{ $user->fullName() }}
                                        <div class="sub header">{{$user->position->getName()}}</div>
                                        <br />
                                        <button onclick="document.getElementById('imgInp').click(); " class="ui inverted black button" type="button" >Сменить фото</button>
                                    </div>
                                </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
    {!! Form::open(['method' => 'POST','id'=>'avatar-form','enctype'=>'multipart/form-data', 'route' => ['avatar.update']]) !!}
    <input type="file" style="display: none;" id="imgInp" name="avatar" accept="image/*" onchange="this.form.submit()" />
    {!! Form::close() !!}
@endsection