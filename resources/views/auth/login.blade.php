@extends('layouts.auth')

@section('content')

    <div class="column">

        <h2 class="ui teal image header">
             <img src="{{url('img/approval-logo.png')}}" />
            @lang('global.system_title')
        </h2>
        <form class="ui large form" method="post" action="{{ url('login') }}">
            @csrf
            <div class="ui stacked login_widget">

                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="name" placeholder="Имя пользователя" value="{{ old('name') }}" />
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Пароль" />
                    </div>
                </div>
                    <div class="inline field">
                        <div class="ui checkbox">
                            <input type="checkbox" tabindex="0" name="remember" class="hidden">
                            <label>Запомнить</label>
                        </div>
                    </div>
                <button type="submit" class="ui fluid large teal submit button">Вход</button>
            </div>

            <div class="ui error message">
                @if (count($errors) > 0)
                        <div class="header">
                            There were problems with input
                        </div>
                        @foreach ($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                @endif
            </div>

        </form>

    <!--     <div class="ui message">
            <a href="{{ route('auth.password.reset') }}">Forgot your password?</a>
        </div> -->
    </div>

@endsection