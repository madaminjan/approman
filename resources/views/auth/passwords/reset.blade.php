@extends('layouts.auth')

@section('content')

    <div class="column">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were problems with input:
                <br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form class="ui form" method="POST"  action="{{ url('password/reset') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" name="token" value="{{ $token }}"/>

            <div class="field">
                <label>E-mail</label>
                <input type="email" name="email" value="{{ old('email') }}" />
            </div>

            <div class="field">
                <label>Password</label>
                <input type="password" name="password"/>
            </div>

            <div class="form-group">
                <label>Confirm password</label>
                <input type="password" name="password_confirmation" />
            </div>

            <button type="submit" class="ui primary button" style="margin-right: 15px;">
                        Reset password
                    </button>

        </form>

    </div>

@endsection
