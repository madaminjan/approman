@extends('layouts.admin')

@section('content')
    <h3 class="ui top attached segment">@lang('global.position')</h3>
    <div class="ui attached segment">
    {!! Form::open(['method' => 'POST','id'=>'inp-form','class'=>'ui form', 'route' => ['admin.position.store']]) !!}

            <div class="field">
                <label>@lang('global.name_uz')</label>
                {!! Form::text('name_uz', old('name_uz'), ['placeholder' => '', 'required' => 'required']) !!}
            </div>
            <div class="field">
                <label>@lang('global.name_ru')</label>
                {!! Form::text('name_ru', old('name_ru'), ['placeholder' => '', 'required' => 'required']) !!}
            </div>

        @include('partials.formerrors')
    {!! Form::submit(trans('global.app_save'), ['class' => 'ui positive button']) !!}
    <a href="{{route('admin.department.index')}}" class="ui button">Отмена</a>
    {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/jquery.validate.min.js') }}"></script>
    <script src="{{ url('js/localization/messages_ru.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#inp-form').validate();

        });

    </script>
@endsection

