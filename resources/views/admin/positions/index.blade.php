@inject('request', 'Illuminate\Http\Request')
@extends('layouts.admin')

@section('content')
    <h5 class="ui top attached header">
        @lang('global.positions')
    </h5>
    <div class="ui attached segment">
        <a href="{{ route('admin.position.create') }}" class="ui mini vk button">@lang('global.app_add_new')</a>
        <table class="ui celled table table-bordered table-striped {{ count($list) > 0 ? 'datatable' : '' }} dt-select">
            <thead>
            <tr>
                {{--<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>--}}
                <th>@lang('global.name_uz')</th>
                <th>@lang('global.name_ru')</th>
                <th>@lang('global.count')</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <tbody>
            @if (count($list) > 0)
                @foreach ($list as $item)
                    <tr data-entry-id="{{ $item->id }}">
                        <td>{{ $item->name_uz }}</td>
                        <td>{{ $item->name_ru}}</td>
                        <td>{{ $item->users()->count()}}</td>
                        <td>
                            <a href="{{ route('admin.position.edit',[$item->id]) }}" class="ui mini facebook button">@lang('global.app_edit')</a>
                            {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['admin.position.destroy', $item->id])) !!}
                            {!! Form::submit(trans('global.app_delete'), array('class' => 'ui mini red button')) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4">@lang('global.app_no_entries_in_table')</td>
                </tr>
            @endif
            </tbody>
        </table>

    </div>
@stop

@section('javascript') 
    <script>

    </script>
@endsection