@inject('request', 'Illuminate\Http\Request')
@extends('layouts.admin')
@section('stylesheet')
    <style>
        .subdept {
            padding-left: 15px !important;
        }
    </style>
@endsection
@section('content')
    <h5 class="ui top attached header">
        @lang('global.departments')
    </h5>
    <div class="ui attached segment">
        <a href="{{ route('admin.department.create') }}" class="ui mini vk button">@lang('global.app_add_new')</a>
        <table class="ui celled table table-bordered table-striped {{ count($list) > 0 ? 'datatable' : '' }} dt-select">
            <thead>
            <tr>
                {{--<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>--}}
                <th>@lang('global.name')</th>
                <th>@lang('global.head')</th>
                {{--<th>@lang('global.employees')</th>--}}
                <th>&nbsp;</th>
            </tr>
            </thead>

            <tbody>
            @if (count($list) > 0)
                @foreach ($list as $item)
                    <tr data-entry-id="{{ $item->id }}">
                        <td>
                            <h4 class="ui header">
                                <i class="folder outline icon"></i>
                                <div class="content">
                                   {{$item->name_uz}}
                                    <div class="sub header">{{$item->name_ru}}</div>
                                </div>
                            </h4>
                        </td>
                        <td>{{ ($item->head) ? $item->head->shortName() : "-не назначен-"}}</td>
{{--                        <td>{{ $item->users_count}}</td>--}}
                        <td>
                            <a href="{{ route('admin.department.edit',[$item->id]) }}" class="ui mini facebook button">@lang('global.app_edit')</a>
                            {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['admin.department.destroy', $item->id])) !!}
                            {!! Form::submit(trans('global.app_delete'), array('class' => 'ui mini red button')) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    {{--subs--}}
                    @foreach($item->departments as $sub)
                        <tr data-entry-id="{{ $sub->id }}">
                            <td>
                                <h4 class="ui header subdept">
                                    <i class="file outline icon"></i>
                                    <div class="content">
                                        {{$sub->name_uz}}
                                        <div class="sub header">{{$sub->name_ru}}</div>
                                    </div>
                                </h4>
                            </td>
                            <td>{{ ($sub->head) ? $sub->head->shortName() : "-не назначен-"}}</td>
{{--                            <td>{{ $sub->users_count}}</td>--}}
                            <td>
                                <a href="{{ route('admin.department.edit',[$sub->id]) }}" class="ui mini facebook button">@lang('global.app_edit')</a>
                                {!! Form::open(array(
                                    'style' => 'display: inline-block;',
                                    'method' => 'DELETE',
                                    'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                    'route' => ['admin.department.destroy', $sub->id])) !!}
                                {!! Form::submit(trans('global.app_delete'), array('class' => 'ui mini red button')) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @endforeach
            @else
                <tr>
                    <td colspan="4">@lang('global.app_no_entries_in_table')</td>
                </tr>
            @endif
            </tbody>
        </table>

    </div>
@stop

@section('javascript') 
    <script>

    </script>
@endsection