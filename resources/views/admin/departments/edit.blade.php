@extends('layouts.admin')

@section('content')
    <h3 class="ui top attached segment">@lang('global.structural_entity')</h3>
    <div class="ui attached segment">
    {!! Form::model($department,['method' => 'PUT','id'=>'inp-form','class'=>'ui form', 'route' => ['admin.department.update',$department->id]]) !!}

        <div class="two fields">
            <div class="field">
                <label>@lang('global.type')</label>
                {!! Form::select('type', $types, old('type'), [ 'required' => 'required','id'=>'type-select']) !!}
            </div>
            <div class="field" style="display: none;" id="parent-block">
                <label>@lang('global.division')</label>
                {!! Form::select('parent_id', $departments, old('parent_id')) !!}
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                <label>@lang('global.name_uz')</label>
                {!! Form::text('name_uz', old('name_uz'), ['placeholder' => '', 'required' => 'required']) !!}
            </div>
            <div class="field">
                <label>@lang('global.name_ru')</label>
                {!! Form::text('name_ru', old('name_ru'), ['placeholder' => '', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                <label>@lang('global.code')</label>
                {!! Form::text('code', old('code'), ['placeholder' => '','autocomplete'=>'off']) !!}
            </div>
            <div class="field">
                <label>@lang('global.head')</label>
                {!! Form::select('head_id', $users, old('head_id')) !!}
            </div>
        </div>
        @include('partials.formerrors')
    {!! Form::submit(trans('global.app_save'), ['class' => 'ui positive button']) !!}
    <a href="{{route('admin.department.index')}}" class="ui button">Отмена</a>
    {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/jquery.validate.min.js') }}"></script>
    <script src="{{ url('js/localization/messages_ru.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#inp-form').validate();

        });
        $(document).on('change','#type-select',function () {
           var type=$(this).val();
           if(type=='DIV') {
               $('#parent-block').hide();
           }else {
               $('#parent-block').show();
           }
        });
    </script>
@endsection

