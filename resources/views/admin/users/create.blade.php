@extends('layouts.admin')

@section('content')
    <h3 class="ui top attached segment">@lang('global.users.title')</h3>
    <div class="ui attached segment">
    {!! Form::open(['method' => 'POST','id'=>'inp-form','class'=>'ui form', 'route' => ['admin.users.store']]) !!}
        <div class="three fields">
            <div class="field">
                <label>@lang('global.users.fields.last_name')</label>
                {!! Form::text('last_name', old('last_name'), ['placeholder' => '', 'required' => 'required']) !!}
            </div>
            <div class="field">
                <label>@lang('global.users.fields.first_name')</label>
                {!! Form::text('first_name', old('first_name'), ['placeholder' => '', 'required' => 'required']) !!}
            </div>
            <div class="field">
                <label>@lang('global.users.fields.mid_name')</label>
                {!! Form::text('mid_name', old('mid_name'), ['placeholder' => '']) !!}
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                <label>@lang('global.division')/@lang('global.department')</label>
                <select name="department_id">
                    <option>@lang('global.not_selected')</option>
                    @foreach($departments as $department)
                        <option class="parent" value="{{$department->id}}">{{$department->getName()}}</option>
                        @if($department->departments()->count()>0)
                            @foreach($department->departments as $child)
                                <option value="{{$child->id}}" class="child">{{$child->getName()}}</option>
                            @endforeach
                        @endif
                    @endforeach
                </select>
                {{--{!! Form::select('department_id', $departments, old('department_id'), [ 'required' => '']) !!}--}}
            </div>
            <div class="field">
                <label>@lang('global.users.fields.position')</label>
                {!! Form::select('position_id', $positions, old('position_id'), [ 'required' => '']) !!}
            </div>
            <div class="field">
                <label>@lang('global.users.fields.phone')</label>
                {!! Form::text('phone', old('phone'), ['placeholder' => '']) !!}
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                <label>Логин</label>
                {!! Form::text('name', old('name'), ['placeholder' => '', 'required' => '','autocomplete'=>"new-password"]) !!}
            </div>
            <div class="field">
                <label>e-mail</label>
                {!! Form::email('email', old('email'), [ 'placeholder' => '', 'required' => '']) !!}
            </div>
        </div>
        <div class="two fields">
            <div class="field">
                <label>Пароль</label>
                {!! Form::password('password', ['placeholder' => '','id'=>'password', 'required' => 'required','autocomplete'=>"new-password"]) !!}
            </div>
            <div class="field">
                <label>Повторите пароль</label>
                <input type="password" name="confirm_password" id="confirm_password" required="required" />
            </div>
        </div>
        <div class="field">
            <label>Роли</label>
            {!! Form::select('roles', $roles, old('roles'), [ 'required' => '']) !!}
        </div>

    {!! Form::submit(trans('global.app_save'), ['class' => 'ui positive button']) !!}
    <a href="{{route('admin.users.index')}}" class="ui button">Отмена</a>
    {!! Form::close() !!}
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/jquery.validate.min.js') }}"></script>
    <script src="{{ url('js/localization/messages_ru.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('#inp-form').validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    }
                }
            });
        });
    </script>
@endsection

