@extends('layouts.admin')

@section('content')
    <h3 class="ui top attached segment">@lang('global.users.title')</h3>
    <div class="ui attached segment">
        {!! Form::model($user, ['method' => 'PUT','class'=>'ui form', 'route' => ['admin.users.update', $user->id]]) !!}
        <div class="field">
            <label>Логин</label>
            {!! Form::text('name', old('name'), ['placeholder' => '', 'required' => '']) !!}
              <p class="help-block"></p>
                        @if($errors->has('name'))
                            <p class="help-block">
                                {{ $errors->first('name') }}
                            </p>
                        @endif
        </div>
        <div class="field">
             <label>e-mail</label>
             {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
             <p class="help-block"></p>
             @if($errors->has('email'))
                 <p class="help-block">
                   {{ $errors->first('email') }}
                 </p>
             @endif
         </div>
         <div class="field">
                <label>Пароль</label>
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('password'))
                    <p class="help-block">
                        {{ $errors->first('password') }}
                    </p>
                @endif
           </div>
           <div class="field">
                <label>Роли</label>
               {!! Form::select('roles', $roles, old('roles') ? old('roles') : $user->roles()->pluck('name', 'name'), ['required' => '']) !!}
               <p class="help-block"></p>
               @if($errors->has('roles'))
                   <p class="help-block">
                      {{ $errors->first('roles') }}
                   </p>
               @endif
           </div>
        {!! Form::submit(trans('global.app_update'), ['class' => 'ui positive button']) !!}
        <a href="{{route('admin.users.index')}}" class="ui button">Отмена</a>
        {!! Form::close() !!}
        </div>
@stop

