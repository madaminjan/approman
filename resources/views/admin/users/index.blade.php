@inject('request', 'Illuminate\Http\Request')
@extends('layouts.admin')

@section('content')
    <h5 class="ui top attached header">
        @lang('global.users.title')
    </h5>
    <div class="ui attached segment">
        <a href="{{ route('admin.users.create') }}" class="ui mini vk button">@lang('global.app_add_new')</a>
        <table class="ui celled table table-bordered table-striped {{ count($users) > 0 ? 'datatable' : '' }} dt-select">
            <thead>
            <tr>
                {{--<th style="text-align:center;"><input type="checkbox" id="select-all" /></th>--}}
                <th>ФИО</th>
                <th>@lang('global.users.fields.name')</th>
                <th>@lang('global.department')</th>
                <th>@lang('global.users.fields.email')</th>
                <th>@lang('global.users.fields.roles')</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <tbody>
            @if (count($users) > 0)
                @foreach ($users as $user)
                    <tr data-entry-id="{{ $user->id }}">
                        <td>{{ $user->shortName() }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->department->getName() }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @foreach ($user->roles()->pluck('name') as $role)
                                <span class="ui green basic label">{{ $user->roleName($role) }}</span>
                            @endforeach
                        </td>
                        <td>
                            <a href="{{ route('admin.users.edit',[$user->id]) }}" class="ui mini facebook button">@lang('global.app_edit')</a>
                            {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['admin.users.destroy', $user->id])) !!}
                            {!! Form::submit(trans('global.app_delete'), array('class' => 'ui mini red button')) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="4">@lang('global.app_no_entries_in_table')</td>
                </tr>
            @endif
            </tbody>
        </table>

    </div>


@stop

@section('javascript') 
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('admin.users.mass_destroy') }}';
    </script>
@endsection