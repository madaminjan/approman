@extends('layouts.admin')

@section('content')
    <h4 class="ui top attached header">
        <div>@lang('global.document_type')</div>
    </h4>
    {!! Form::model($doctype,['method' => 'PUT','class'=>'ui form','enctype'=>'multipart/form-data', 'route' => ['doctypes.update',$doctype->id]]) !!}
        <div id="approvers-block">
            @if($doctype->approvers)
                @foreach($doctype->approvers as $approver)
                <input type="hidden" class="approver_input" name="approvers[]" value="{{$approver->id}}" />
                @endforeach
            @endif
        </div>
        <div class="ui attached segment">
            <div class="two fields">
                <div class="field">
                    <label>@lang('global.name_uz')</label>
                    {!! Form::text('name_uz', old('name_uz'), ['placeholder' => '', 'required' => '']) !!}
                </div>
                <div class="field">
                    <label>@lang('global.name_ru')</label>
                    {!! Form::text('name_ru', old('name_ru'), ['placeholder' => '', 'required' => '']) !!}
                </div>
            </div>


                <div class="field">
                    <div class="ui checkbox">
                        {{ Form::checkbox('has_template', 1, $doctype->has_template,['id'=>'has_template']) }}
                        {{--<input type="checkbox" tabindex="0" class="hidden" name="has_template" id="has_template" value="1" />--}}
                        <label>@lang('global.has_template')</label>
                    </div>
                </div>
                @if($doctype->has_template)
                    <div class="two fields file-block">
                        <div class="field">
                            <h4 class="ui image header">
                                <img src="/img/filetypes/{{ $doctype->getFirstMedia('templates')->extension}}.png" class="ui image">
                                <div class="content">
                                    <a href="/template/download/{{$doctype->getFirstMedia('templates')->id}}">{{ $doctype->getFirstMedia('templates')->file_name }}</a>
                                    <div class="sub header">{{ $doctype->getFirstMedia('templates')->human_readable_size }}</div>
                            </h4>
                        </div>
                        <div class="field">
                            <button type="button" class="ui button" id="change-attachment">Заменить</button>
                            <input class="form-field" type="file" name="attachment" id="attachment-field" style="display: none;" />
                        </div>
                    </div>
                @else
                    <div class="field file-block" {!! $doctype->has_template==0 ? 'style="display:none;"' : ''  !!} >
                        <input class="form-field" type="file" name="attachment" />
                    </div>

                @endif

            <h5 class="ui dividing teal header">@lang('global.document_route')</h5>
            <div class="two fields">
                <div class="field">
                    <div class="ui checkbox">
                        {{ Form::checkbox('chief_approval', 1, $doctype->chief_approval,['id'=>'chief-approval']) }}
                        {{--<input type="checkbox" tabindex="0" class="hidden" name="chief_approval" id="chief-approval" value="1" />--}}
                        <label>@lang('global.chief_approval')</label>
                    </div>
                </div>
                <div class="inline fields">
                    <label for="fruit">Вид:</label>
                    <div class="field">
                        <div class="ui radio checkbox">
                            {{ Form::radio('seq', 'SEQ', $doctype->seq=='SEQ',['class'=>'seq']) }}
                            {{--<input type="radio" name="seq" checked="" tabindex="0" class="hidden seq" value="SEQ" />--}}
                            <label>Последовательное</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui radio checkbox">
                            {{ Form::radio('seq', 'PAR', $doctype->seq=='PAR',['class'=>'seq']) }}
                            {{--<input type="radio" name="seq" tabindex="0" class="hidden seq" value="PAR" />--}}
                            <label>Параллельное</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui ordered small computer {{$doctype->seq=='SEQ' ? 'stackable' : ''}} steps" id="steps">
                @if($doctype->chief_approval)
                    <div class="step" id="boss-step">
                        <div class="content">
                            <div class="title">Руководитель</div>
                            <div class="description">Согласовать с руководителем</div>
                        </div>
                    </div>
                @endif
                @if($doctype->approvers())

                    @foreach($doctype->approvers as $approver)
                            <div class="step" user-id="{{$approver->id}}">
                                <div class="content">
                                    <div class="title">{{$approver->shortName()}}<i class="circular times red small icon link remove-step"></i></div>
                                    <div class="description">{{$approver->position->getName()}}</div>
                                </div>
                            </div>
                    @endforeach
                @endif
            </div>
            <div class="field">
                <button type="button" class="ui small basic small button" id="add-point">@lang('global.add_approver')</button>
            </div>
        </div>
        <div class="ui bottom attached segment">
            {!! Form::submit(trans('global.app_save'), ['class' => 'ui positive button']) !!}
            <a href="{{route('doctypes.index')}}" class="ui button">Отмена</a>
      </div>
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="ui small modal" id="routepoint-modal">
        <div class="header">@lang('global.add_approver')</div>
        <div class="content">
            <div class="ui form">
                <div class="field">
                    <select name="points" id="point-select">
                        <option value="">--Выберите согласующего--</option>
                        @foreach($users as $user)
                            <option value="{{$user->id}}"
                                    sname="{{$user->shortName()}}" job="{{$user->position->getName()}}">
                                {{$user->fullName()}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="actions">
            <div class="ui approve green small button">@lang('global.app_save')</div>
            <div class="ui cancel small button">@lang('global.app_cancel')</div>
        </div>
    </div>
@stop

@section('javascript')
    <script src="{{ url('js/jquery.validate.min.js') }}"></script>
    <script src="{{ url('js/localization/messages_ru.min.js') }}"></script>
<script>

    $('form').validate();

    $(document).on('change','#chief-approval',function() {
        if($(this).is(':checked')) {
            var text='<div class="step" id="boss-step"><div class="content"><div class="title">Руководитель</div><div class="description">Согласовать с руководителем</div></div></div>';
            $('#steps').prepend(text);
        }else{
            $('#boss-step').remove();
        }
    });

    $(document).on('click','#add-point',function(){
        $('#point-select').val("");
        $('#routepoint-modal').modal({
            onApprove : function() {
                return savePoint(false);
            }
        }).modal('show');

    });

    function savePoint() {
        var selId=$('#point-select').val().trim();

        if(!selId) {
            return false;
        }

        var name=$('#point-select option:selected').attr('sname');
        var job=$('#point-select option:selected').attr('job');
        var text='<div class="step" user-id="'+selId+'"><div class="content"><div class="title">'+name+'<i class="circular times red small icon link remove-step"></i></div><div class="description">'+job+'</div></div></div>';
        if($('#performer-step').length>0) {
            $(text).insertBefore('#performer-step');
        }else{
            $('#steps').append(text);
        }
        $('#approvers-block').append('<input type="hidden" class="approver_input" name="approvers[]" value="'+selId+'" />');

        $('#point-select option[value='+selId+']').hide();
    }
    $(document).on('click','.remove-step',function(e) {
        var step=$(this).closest('.step');
        var uid = step.attr('user-id');
        step.remove();
        $('input.approver_input[value='+uid+']').remove();
        $('#point-select option[value='+uid+']').show();
    });
    $(document).on('change','#has_template',function() {
        if($(this).is(':checked')) {
            $('.file-block').show();
        }else{
            $('.file-block').hide();
        }
    });
    $(document).on('change','.seq',function (e) {
        var seq=$('input[name="seq"]:checked').val();
        $('#steps').removeClass('ordered');
        if(seq=='SEQ') $('#steps').addClass('ordered');
    });
    $(document).on('click','#change-attachment',function () {
       $('#attachment-field').show();
       $(this).hide();
    });
</script>
@endsection

