@extends('layouts.app')

@section('content')
    <!-- Content area -->
    @if (count($list)== 0)
        <div class="ui placeholder segment">
            <div class="ui icon header">
                <i class="folder open outline icon"></i>
                Нет записей
            </div>

            <a href="{{route('drafts.create')}}" class="ui primary button">Добавить документ</a>
        </div>
   @else
   <div class="ui segment">
        <table class="ui very basic small table">
            <thead>
            <tr>
                <th>Детали</th>
                <th>Согласующие</th>
                <th>Комментарии</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $item)
                <tr item-id="{{$item->id}}">
                    <td class="name">
                        <h4 class="ui image header">
                            <img src="/img/filetypes/{{$item->getFirstMedia('attachment')->extension}}.png" class="ui image">
                            <div class="content">
                                <a href="{{route('drafts.show',$item->id)}}">{{$item->subject}}</a>
                                <div class="sub header">{{$item->regDate}}, автор: {{$item->user->shortName()}}, Версия: {{$item->version}}</div>
                                <div class="sub header"> <a href="/template/download/{{$item->getFirstMedia('attachment')->id}}">{{$item->getFirstMedia('attachment')->name}}, {{$item->getFirstMedia('attachment')->human_readable_size}}</a></div>
                            </div>
                        </h4>
                    </td>
                    <td>
                        <div class="ui selection list">
                            @foreach($item->approvers as $approver)
                                <a class="item">
                                    <i class="help icon"></i>
                                    <div class="content">
                                    {{--<div class="ui orange horizontal small basic label">На ожидании</div>--}}
                                    {{ $approver->shortName() }}
                                    </div>
                                </a >
                            @endforeach
                        </div>
                        {{--<a class="ui basic orange small label">На согласовании</a>--}}
                    </td>
                    <td>
                        <button class="ui mini button comment-link">{{$item->all_comments_count}}
                            {{--<div class="floating ui red tiny label">5</div>--}}
                        </button>
                    </td>
                    <td class="right aligned">
                        @can('edit',$item)
                        <a href="{{ route('drafts.edit',[$item->id]) }}" class="ui mini icon button" data-tooltip="@lang('global.app_edit')" ><i class="edit icon"></i></a>
                        @endcan
                        @can('delete',$item)
                        {!! Form::open(array(
                            'style' => 'display: inline-block;',
                            'method' => 'DELETE',
                            'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                            'route' => ['drafts.destroy', $item->id])) !!}
                        {{--{!! Form::submit(trans('global.app_delete'), array('class' => 'ui mini red button')) !!}--}}
                        <button type="submit" class="ui mini red icon button" data-tooltip="@lang('global.app_delete')"><i class="trash alternate icon"></i></button>
                        {!! Form::close() !!}
                        @endcan
                            <a class="ui mini button" role="button" href="{{route('drafts.show',$item->id)}}">Детали</a>
                       {{-- <div class="ui right pointing dropdown icon button" tabindex="0">
                            <i class="ellipsis vertical icon"></i>
                            <div class="menu left transition hidden" tabindex="-1">
                                <div class="item edit-doc"><i class="edit icon"></i> Просмотр</div>
                                <div class="item del-item"><i class="delete icon"></i> Удалить</div>
                            </div>
                        </div>--}}
                    </td>
                </tr>
            @endforeach

                {{--    <tr item-id="98">
                        <td class="name">		   		<h4 class="ui image header">
                                <img src="{{asset('img/filetypes1/docx.png')}}" class="ui image">
                                <div class="content"><a id="98" class="file_lnk " ext="docx" href="openfile.php?aku=ZmlsZW5hbWU9ZG9jdW1lbnRzL3Byb2plY3QvOTguZG9jeA==">20.06. ИНФОРМАЦИЯ  по объет   .docx</a>	               <div class="sub header">Загружено 21.06.2018 09:49, автор: 42Kb	               </div>
                                </div></h4>

                        </td>
                        <td>
                            <a class="ui basic orange label">На согласовании</a>
                        </td>
                        <td class="right aligned"><button class="ui icon button del-item"><i class="trash icon"></i></button></td>
                    </tr>
                 --}}
                  </tbody>
                </table>
                </div>
    @endif
@endsection