@foreach($comments as $comment)
    <div class="comment">
        <a class="avatar">
            <img  src="{{route('common.avatar.thumb',$comment->user->id)}}" />
        </a>
        <div class="content">
            <a class="author">{{ $comment->user->shortName() }}</a>
            <div class="metadata">
                <span class="date">{{ $comment->created_at->diffForHumans() }}</span>
            </div>
            <div class="text">
                {{ $comment->body }}
            </div>
            <div class="actions">
                <a href="#" class="reply" item-id="{{$comment->id}}">Ответить</a>
            </div>
        </div>
        @if($comment->replies->count()>0)
            <div class="comments">
                @include('drafts.commentsDisplay', ['comments' => $comment->replies])
            </div>
        @endif
    </div>
@endforeach