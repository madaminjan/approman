@extends('layouts.app')
@section('stylesheet')
    <link href="{{ asset('select2/css/select2.min.css') }}" rel="stylesheet" />
    @endsection
@section('content')
    <!-- Content area -->
    <div class="ui container"">
        <div class="ui top attached secondary segment">
            <h4>@lang('global.app_add_new')</h4>
        </div>
        <div class="ui attached segment">
            {!! Form::model($draft, ['method' => 'PUT','class'=>'ui form', 'enctype'=>'multipart/form-data','route' => ['drafts.update', $draft->id]]) !!}
                <input type="hidden" name="id" value="{{$draft->id}}" />
                <div class="field">
                    <label>@lang('global.drafts.type')</label>
                    {!! Form::select('document_type_id', $types, old('document_type_id'), ['id'=>'type-select','disabled'=>'true', 'required' => 'required']) !!}
                </div>
                <div class="field">
                    <label>@lang('global.drafts.subject')</label>
                    {!! Form::text('subject', old('subject'), ['placeholder' => '', 'required' => 'required']) !!}
                </div>
            <div class="two fields file-block">
                <div class="field">
                    <h4 class="ui image header">
                        <img src="/img/filetypes/{{ $draft->getFirstMedia('attachment')->extension}}.png" class="ui image">
                        <div class="content">
                            <a href="{{route('drafts.media',$draft->id)}}">{{ $draft->getFirstMedia('attachment')->file_name }}</a>
                            <div class="sub header">{{ $draft->getFirstMedia('attachment')->human_readable_size }}</div>
                    </h4>
                </div>
                <div class="field">
                    <button type="button" class="ui button" id="change-attachment">Заменить</button>
                    <input class="form-field" type="file" name="attachment" id="attachment-field" style="display: none;" />
                </div>
            </div>


            <h5 class="ui dividing header">@lang('global.signer_list')</h5>
            <div id="signers-block" class="field">
                <div class="ui ordered small computer {!! $draft->type->seq=='SEQ' ? 'stackable' : '' !!} steps" id="steps">
                    @foreach($draft->approvers as $approver)
                    <div class="step">
                        <div class="content">
                            <div class="title">{{$approver->name}}</div>
                            <div class="description">{{$approver->position->getName()}}</div>
                            </div>
                        </div>
                    @endforeach
            </div>

            <div class="ui segment">
                <button class="ui teal button" type="submit">@lang('global.app_save')</button>
                <a class="ui button" href="{{route('drafts.index')}}">@lang('global.app_back_to_list')</a>
            </div>
            {!! Form::close() !!}
            @include('partials.formerrors')
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ url('js/jquery.validate.min.js') }}"></script>
    <script src="{{ url('js/localization/messages_ru.min.js') }}"></script>
    <script src="{{ asset('select2/js/select2.min.js') }}"></script>
    <script>
        $('form').validate();
        $(document).on('click','#change-attachment',function () {
            $('#attachment-field').show();
            $(this).hide();
        });
    </script>


    @endsection

