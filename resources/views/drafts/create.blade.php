@extends('layouts.app')
@section('stylesheet')
    <link href="{{ asset('select2/css/select2.min.css') }}" rel="stylesheet" />
    @endsection
@section('content')
    <!-- Content area -->
    <div class="ui container"">
        <div class="ui top attached secondary segment">
            <h4>@lang('global.app_add_new')</h4>
        </div>
        <div class="ui attached segment">
            {!! Form::open(['method' => 'POST','id'=>'inp-form','class'=>'ui form','enctype'=>'multipart/form-data', 'route' => ['drafts.store']]) !!}
                <div class="field">
                    <label>@lang('global.drafts.type')</label>
                    {!! Form::select('document_type_id', $types, old('document_type_id'), ['id'=>'type-select', 'required' => 'required']) !!}
                </div>
                <div class="field">
                    <label>@lang('global.drafts.subject')</label>
                    <input type="text" name="subject" required="required" />
                </div>
            <div class="fields">
                <div class="twelve wide field">
                    <input type="file" name="attachment" required="required" />
                </div>
                <div class="four wide field" id="download-button-block"></div>
            </div>
            <h5 class="ui dividing header">@lang('global.signer_list')</h5>
            <div id="signers-block" class="field"></div>
            <div class="ui segment">
                <button class="ui teal button" type="submit">@lang('global.app_save')</button>
                <a class="ui button" href="{{route('drafts.index')}}">@lang('global.app_back_to_list')</a>
            </div>
            {!! Form::close() !!}
            @include('partials.formerrors')
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ url('js/jquery.validate.min.js') }}"></script>
    <script src="{{ url('js/localization/messages_ru.min.js') }}"></script>
    <script src="{{ asset('select2/js/select2.min.js') }}"></script>
    <script>
        $('form').validate();
        $(document).on('change','#type-select',function () {
           let typeId=$(this).val();
           if(typeId=="") {
               $('#signers-block').html('');
               return false;
           }
            setWaitState(true);
            $.ajax({
                type: 'get',
                url: '/drafts/typedata/'+typeId,
                success: function(res){
                    var apprs=res.approvers;
                    var data='<div class="ui ordered small computer '+(res.seq=='SEQ' ? 'stackable ':'')+'steps" id="steps">';
                    $(apprs).each(function (i, item) {
                        console.log(item);
                        data+=' <div class="step">\n' +
                            '<div class="content">\n' +
                            '<div class="title">'+item.name+'</div>\n' +
                            '<div class="description">'+item.post+'</div>\n' +
                            '</div>\n' +
                            '</div>';
                    });
                    data+='</div>';
                    $('#signers-block').html(data);
                    if(res.hastmp) {
                        var btn='<a class="ui teal button" href="'+res.url+'">@lang("global.download_template")</a>';
                        $('#download-button-block').append(btn);
                    }
                },
                error: function(data){
                    var errors = data.responseJSON;
                    alert(errors.errors);
                    return false;
                },
                complete: function() {
                    setWaitState(false);
                }
            });



        });


    </script>


    @endsection

