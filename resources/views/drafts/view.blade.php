@extends('layouts.app')
@section('content')
    <!-- Content area -->
    <div class="ui container">
        <div class="ui top attached secondary segment">
            <h4>@lang('global.app_view')</h4>
        </div>
        <div class="ui attached segment">
            <div class="ui items">
                <div class="item">
                    <div class="ui tiny image">
                        <img  src="{{route('common.avatar.thumb',$draft->user->id)}}" />
                    </div>
                    <div class="content">
                        <a class="header">{{$draft->subject}}</a>
                        <div class="meta">
                            <span class="cinema">{{$draft->regDate}}, {{$draft->user->shortName()}}, Версия: {{$draft->version}} </span>
                        </div>
                        <div class="description">
                            <p>Тип: {{$draft->type->getName()}}</p>
                        </div>
                        <div class="extra">
                            <a class="ui label" href="{{route('drafts.media',$draft->id)}}"><i class="file alternate icon"></i> {{$draft->getFirstMedia('attachment')->file_name}}</a>
                            <div class="{{$draft->getStatusClass()}}">{{$draft->statusname()}}</div>
                            <a href="{{route('drafts.edit',$draft->id)}}" class="ui label">Изменить</a>
                        </div>
                    </div>
                </div>
            </div>


           <h4>Маршрут согласования</h4>
            <table class="ui small table">
                <thead>
                <tr>
                    <th>@lang('global.approver_name')</th>
                    <th>@lang('global.read_at')</th>
                    <th>@lang('global.status')</th>
                    <th>@lang('global.approved_at')</th>
                    <th>@lang('global.actions')</th>
                </tr>
                </thead>
                @foreach($draft->approvers as $approver)
                    <tr item-id="{{$approver->id}}">
                        <td>{{$approver->shortName()}}</td>
                        <td>{{$approver->pivot->read_at}}</td>
                        <td>{!! $draft->approverStatus($approver->pivot->status)  !!}</td>
                        <td>{{$approver->pivot->approved_at}}</td>
                        <td>
                            @if($approver->pivot->status=='PENDING' && $approver->id==Auth::user()->id)
                                @if($approver->pivot->read_at==null)
                                    <input type="hidden" id="make-read" value="{{$approver->id}}" />
                                @endif
                                {!! Form::open(['method' => 'POST','id'=>'approve-form', 'route' => ['drafts.approve']]) !!}
                                    <input type="hidden" name="draft" value="{{$draft->id}}" />
                                    <input type="hidden" name="approver" value="{{$approver->id}}" />
                                    <button type="submit" class="ui left attached icon green small button approver" data-tooltip="Утвердить"><i class="thumbs up icon"></i></button>
                                    <button type="button" class="ui right attached icon orange small button rejector" data-tooltip="Отвергнуть с комментарием"><i class="thumbs down icon"></i></button>
                                 {!! Form::close() !!}
                                {{--rejector modal--}}
                                    <div class="ui small modal" id="reject-comment-modal">
                                        <div class="header">Комментарий</div>
                                        <div class="content">
                                            {!! Form::open(['method' => 'POST','class'=>'ui form validate','id'=>'reject-form', 'route' => ['drafts.reject']]) !!}
                                            <input type="hidden" name="draft" value="{{$draft->id}}" />
                                            <input type="hidden" name="approver" value="{{$approver->id}}" />
                                            <div class="field">
                                                <textarea name="comment" required="required"></textarea>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="actions">
                                            <button type="button" id="reject-draft" class="ui green button">@lang('global.app_save')</button>
                                            <div class="ui negative button">@lang('global.app_cancel')</div>
                                        </div>

                                    </div>
                            @endif
                        </td>
                    </tr>
                    @endforeach
            </table>
            <h3 class="ui dividing header">Комментарии</h3>
            <div class="ui comments">
            @include('drafts.commentsDisplay', ['comments' => $draft->comments])
            </div>
            <button class="ui blue labeled icon small button" id="add-comment">
                <i class="icon edit"></i> Добавить комментарий
            </button>
            <a href="{{\Illuminate\Support\Facades\URL::previous()}}" class="ui button">Назад</a>

        </div>
    </div>
{{--comment modal--}}
    <div class="ui modal" id="comment-modal">
        <div class="header">Комментарий</div>
        <div class="content">
            <form class="ui form validate" id="comment-form" method="post" action="{{route('comments.store')}}">
                @csrf
                <input type="hidden" name="draft_id" value="{{ $draft->id }}" />
                <input type="hidden" name="parent_id" id="parent-id" />
                <div class="field">
                    <label>Текст комментария</label>
                    <textarea name="body" id="comment-body" required="required"></textarea>
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="ui approve button">Сохранить</div>
            <div class="ui cancel button">Отмена</div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{url('/js/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/js/localization/messages_ru.min.js')}}"></script>
    <script>
        var did={{$draft->id}};

        $(document).ready(function () {
            $('.validate').validate();
           if($('#make-read').length==1) {
               let id=$('#make-read').val();
               setWaitState(true);
               $.ajax({
                   type: 'get',
                   url: '/approvals/setread/'+did+'/'+id,
                   success: function(res){
                       if(res.result=='success')
                       {

                       }else{
                           alert(res.message);
                       }
                   },
                   error: function(data){
                       var errors = data.responseJSON;
                       console.log(errors.errors);
                       return false;
                       // Render the errors with js ...
                   },
                   complete: function() {
                       setWaitState(false);
                   }
               });
           }
        });

        $(document).on('click','.rejector',function () {
           $('#reject-comment-modal').modal('show');
        });
        $(document).on('click','#reject-draft',function () {
            $('#reject-form').submit();
        });

        $(document).on('click','#add-comment',function () {
           $('#comment-body').val('');
           $('#parent-id').val('');
           showCommentModal();
        });
// reply
        $(document).on('click','.reply',function (e) {
            e.preventDefault();
            var pid=$(this).attr('item-id');
            $('#comment-body').val('');
            $('#parent-id').val(pid);
            showCommentModal();
        });

        function showCommentModal() {
            $('#comment-modal').modal({
                onApprove: function () {
                    return saveComment();
                }
            }).modal('show');
        }

        function saveComment() {
            if($('#comment-form').valid()) {
                $('#comment-form').submit();
                return true;
            }else{
                return false;
            }
        }
    </script>
    @endsection

