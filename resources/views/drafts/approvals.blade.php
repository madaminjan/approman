@extends('layouts.app')

@section('content')
    <!-- Content area -->
    @if (count($list)== 0)
        <div class="ui placeholder segment">
            <div class="ui icon header">
                <i class="folder open outline icon"></i>
                Нет записей
            </div>

            <a href="{{route('drafts.create')}}" class="ui primary button">Добавить документ</a>
        </div>
   @else
   <div class="ui segment">
        <table class="ui very basic small table">
            <tbody>
            @foreach($list as $item)
                <tr item-id="{{$item->id}}">
                    <td class="name">
                        <h4 class="ui image header">
                            <img src="/img/filetypes/{{$item->getFirstMedia('attachment')->extension}}.png" class="ui image">
                            <div class="content">
                                <a href="/template/download/{{$item->getFirstMedia('attachment')->id}}">{{$item->subject}}"</a>
                                <div class="sub header">{{$item->reg_date}}, автор: {{$item->user->shortName()}}, {{$item->getFirstMedia('attachment')->human_readable_size}}</div>
                            </div>
                        </h4>
                    </td>
                    <td>
                        <a class="ui basic orange small label">На согласовании</a>
                    </td>
                    <td class="right aligned">
                        <div class="ui right pointing dropdown icon button" tabindex="0">
                            <i class="ellipsis vertical icon"></i>
                            <div class="menu left transition hidden" tabindex="-1">
                                <div class="item edit-doc"><i class="edit icon"></i> Просмотр</div>
                                <div class="item del-item"><i class="delete icon"></i> Утвердить</div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach

                {{--    <tr item-id="98">
                        <td class="name">		   		<h4 class="ui image header">
                                <img src="{{asset('img/filetypes1/docx.png')}}" class="ui image">
                                <div class="content"><a id="98" class="file_lnk " ext="docx" href="openfile.php?aku=ZmlsZW5hbWU9ZG9jdW1lbnRzL3Byb2plY3QvOTguZG9jeA==">20.06. ИНФОРМАЦИЯ  по объет   .docx</a>	               <div class="sub header">Загружено 21.06.2018 09:49, автор: 42Kb	               </div>
                                </div></h4>

                        </td>
                        <td>
                            <a class="ui basic orange label">На согласовании</a>
                        </td>
                        <td class="right aligned"><button class="ui icon button del-item"><i class="trash icon"></i></button></td>
                    </tr>
                 --}}
                  </tbody>
                </table>
                </div>
    @endif
@endsection