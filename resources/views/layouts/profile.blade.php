<!DOCTYPE html>
<html>

<head>
    @include('partials.head')
</head>

<body>
@include('partials.header')
<div class="ui content">
    @yield('content')
</div>

@include('partials.footer')
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">Logout</button>
{!! Form::close() !!}

@include('partials.javascripts')
</body>
</html>