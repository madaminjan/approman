<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.head')
    <style type="text/css">
       
        body > .grid {
            height: 100%;
        }
        .image {
            margin-top: -100px;
        }
        .column {
            max-width: 450px;
        }
        </style>
</head>

<body>


<div class="ui middle aligned center aligned grid">
    @yield('content')
</div>

    @include('partials.javascripts')

</body>
</html>