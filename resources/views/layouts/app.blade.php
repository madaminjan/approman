<!DOCTYPE html>
<html>

<head>
    @include('partials.head')
</head>

<body>
@include('partials.header')
<div id="content" class="ui content">
    <div class="ui stackable internally celled doubling grid">
        <!-- Left Sidebar -->
        <div class="three wide column">
            @include('partials.sidebar')
        </div>
        <!-- Middle Content -->
        <div class="twelve wide column">
            @yield('content')
        </div>
    </div>
</div>

@include('partials.footer')
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">Logout</button>
{!! Form::close() !!}

@include('partials.javascripts')
</body>
</html>