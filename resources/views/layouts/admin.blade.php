<!DOCTYPE html>
<html>

<head>
    @include('partials.head')
</head>

<body>
@include('partials.header')
<div id="content" class="content">
    <div class="ui stackable internally celled doubling grid">
        <!-- Left Sidebar -->
        <div class="three wide column">
            @include('partials.admin.sidebar')
        </div>
        <!-- Middle Content -->
        <div class="twelve wide column">
            @if (Session::has('message'))
                <div class="ui positive message">
                    <i class="close icon"></i>
                    <p>{{ Session::get('message') }}</p>
                </div>
            @endif
            @if ($errors->count() > 0)
                <div class="ui negative message">
                        <i class="close icon"></i>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @yield('content')
        </div>
    </div>
</div>

@include('partials.footer')
{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">Logout</button>
{!! Form::close() !!}

@include('partials.javascripts')
</body>
</html>