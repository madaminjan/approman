<div id="top"></div>
<!-- Progress bar area -->
<div class="progress bar"></div>
<a href="#top" class="ui circular icon button" id="toTop" data-slide="slide">
    <i class="angle up icon"></i>
</a>

<!-- Navbar fixed top area -->
<div class="ui fixed top menu">
    <div class="header item">
        <img src="{{url('img/approval-logo.png')}}" />
        <a href="/"> @lang('global.system_title')</a>
    </div>

    <div class="right menu">
        <div class="item">
            <div class="ui icon input">
                <input type="text" placeholder="Поиск...">
                <i class="search link icon"></i>
            </div>
        </div>

        <a class="item" id="messages-button">
            <i class="mail icon"></i><div class="floating ui red circular small label counter-bubble" id="message-bubble" style="display: none;"></div>
        </a>
        <div class="item">
            <div class="ui pointing dropdown link">
                <div class="text"> @lang('global.lang_name')</div>
                <i class="angle down icon"></i>
                <div class="menu">
                    <a class="item" href="{{url('/language/ru')}}">Русский</a>
                    <a class="item" href="{{url('/language/uz')}}">Ўзбекча</a>
                    <a class="item" href="{{url('/language/oz')}}">O`zbekcha</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="ui pointing dropdown link">
                <div class="text">
                    <img class="ui avatar image" src="{{route('avatar.thumb')}}" />
                    {{Auth::user()->shortName()}}
                </div>
                <i class="angle down icon"></i>
                <div class="menu">
                    <a href="{{url('/profile')}}" class="item"><i class="user icon"></i>Профиль</a>
                    <a href="#" class="item logout-lnk"><i class="log out icon"></i>Выйти</a>
                </div>
            </div>
        </div>

        <!-- end right menu -->
    </div>
</div>
<!-- Collapse Navbar Menu -->
<div class="ui fluid vertical menu collapse"></div>
