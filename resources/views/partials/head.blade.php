<meta charset="utf-8">
<title>
    {{ trans('global.global_title') }}
</title>

<meta http-equiv="X-UA-Compatible"
      content="IE=edge">
<meta content="width=device-width, initial-scale=1.0"
      name="viewport"/>
<meta http-equiv="Content-type"
      content="text/html; charset=utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link href="{{ url('semantic/semantic.min.css') }}" rel="stylesheet" />
<link href="{{ url('semantic/components/checkbox.min.css') }}" rel="stylesheet" />
{{--<link href="{{ url('css/owl.carousel.css') }}" rel="stylesheet" />--}}
<link href="{{ url('css/main.css') }}" rel="stylesheet" />
<link href="{{ url('css/HoldOn.min.css') }}" rel="stylesheet" />
@yield('stylesheet')