@inject('request', 'Illuminate\Http\Request')

<div class="ui vertical pointing fluid menu">
    <a href="{{ url('/') }}" class="{{ $request->segment(1) == '' ? 'active' : '' }} item">
        @lang('global.app_dashboard')
    </a>
    <a href="{{ route('admin.department.index') }}" class="{{ $request->segment(2) == 'department' ? 'active active-sub' : '' }} item">
        @lang('global.organization_structure')
    </a>
    <a href="{{ route('admin.position.index') }}" class="{{ $request->segment(2) == 'position' ? 'active active-sub' : '' }} item">
        @lang('global.positions')
    </a>
    <a href="{{ route('admin.users.index') }}" class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }} item">
        @lang('global.employees')
    </a>
    <a href="{{route('doctypes.index')}}" class="{{ $request->segment(1) == 'doctypes' ? 'active' : '' }} item">
        @lang('global.document_types')
    </a>
</div>