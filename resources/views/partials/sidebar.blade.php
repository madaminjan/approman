@inject('request', 'Illuminate\Http\Request')
<div class="ui fluid vertical menu moderns">
    <a href="" class="header item">Категории</a>
    <a href="{{route('drafts.index')}}" class="{{ $request->segment(1) == 'drafts' ? 'active' : '' }} item">
        @if(Auth::user()->drafts()->count()>0)
        <div class="ui small orange label">{{Auth::user()->drafts()->count()}}</div>
        @endif
            Создано мной</a>
    <a href="{{route('drafts.pending')}}" class="{{ $request->segment(1) == 'pending' ? 'active' : '' }} item">
        @if(Auth::user()->pendingApprovals()->count()>0)
            <div class="ui small blue label">{{Auth::user()->pendingApprovals()->count()}}</div>
        @endif
            Мне на утверждение
        </a>
    <a href="{{route('drafts.archived')}}" class="{{ $request->segment(1) == 'archived' ? 'active' : '' }} item">
        Архив
    </a>
</div>
@if($request->segment(2) != 'create')
<a class="ui right floated button" href="{{route('drafts.create')}}">Создать</a>
    @endif
