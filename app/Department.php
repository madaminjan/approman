<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable=['type','name_uz','name_ru','parent_id','code','head_id'];

    public function scopeParents($query)
    {
        return $query->whereNull('parent_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function head()
    {
        return $this->belongsTo(User::class,'head_id','id');
    }

    public function departments()
    {
        return $this->hasMany(Department::class,'parent_id');
    }

    public function getName()
    {
        $locale = \App::getLocale();
        return $locale=='ru' ? $this->name_ru : $this->name_uz;
    }
}
