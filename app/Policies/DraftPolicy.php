<?php

namespace App\Policies;

use App\Draft;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DraftPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(User $user,Draft $draft)
    {
        return $user->id===$draft->user_id;
    }
    public function delete(User $user,Draft $draft)
    {
        return $user->id===$draft->user_id;
    }
}
