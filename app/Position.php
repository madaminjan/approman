<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $fillable=['name_uz','name_ru'];
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function getName()
    {
        $locale=\App::getLocale();
        return $locale=='ru' ? $this->name_ru : $this->name_uz;
    }
}
