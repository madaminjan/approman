<?php

use Illuminate\Support\Facades\Lang;

function timeAgo($date ) // $date --> время в формате Unix time
{
    $date=strtotime($date);
    $stf      = 0;
    $cur_time = time();
    $diff     = $cur_time - $date;

    $seconds = Lang::get('labels.time.seconds');
    $minutes = Lang::get('labels.time.minutes');
    $hours   = Lang::get('labels.time.hours');
    $days    = Lang::get('labels.time.days');
    $weeks   = Lang::get('labels.time.weeks');
    $months  = Lang::get('labels.time.months');
    $years   = Lang::get('labels.time.years');
    $decades = Lang::get('labels.time.decades');
    $ago = Lang::get('labels.time.ago');
    $phrase = array( $seconds, $minutes, $hours, $days, $weeks, $months, $years, $decades );
    $length = array( 1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600 );

    for ( $i = sizeof( $length ) - 1; ( $i >= 0 ) && ( ( $no = $diff / $length[ $i ] ) <= 1 ); $i -- ) {
        ;
    }
    if ( $i < 0 ) {
        $i = 0;
    }
    $_time = $cur_time - ( $diff % $length[ $i ] );
    $no    = floor( $no );
    $value = sprintf( "%d %s ", $no, getPhrase( $no, $phrase[ $i ] ) );

    if ( ( $stf == 1 ) && ( $i >= 1 ) && ( ( $cur_time - $_time ) > 0 ) ) {
        $value .= time_ago( $_time );
    }
    return $value . ' '.$ago;
}

function getPhrase( $number, $titles ) {
    $cases = array( 2, 0, 1, 1, 1, 2 );
    return $titles[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];
}