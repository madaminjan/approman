<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class DocumentType extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable=['name_uz','name_ru','has_template','chief_approval','seq'];

    public function approvers()
    {
        return $this->belongsToMany(User::class,'document_type_approvers');
    }

    public function getName()
    {
        $locale = App::getLocale();
        switch ($locale) {
            case 'uz':
            case 'oz':
                return $this->name_uz;
                break;
            default:
                return $this->name_ru;
        }
    }
}
