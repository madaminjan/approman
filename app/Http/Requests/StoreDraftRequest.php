<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 30.05.2019
 * Time: 2:12
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreDraftRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'subject' =>'required',
            'attachment'=>'required',
            'document_type_id' =>'required'
        ];
    }

}