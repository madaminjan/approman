<?php

namespace App\Http\Controllers;

use App\DocumentType;
use App\User;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;

class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $list=DocumentType::with('approvers')->get();
        return view('doctypes.index',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users=User::where('id','!=',1)->get();
        return view('doctypes.create',compact('users'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $docType=DocumentType::create($request->all());
        if($request->hasFile('attachment')) {
            $docType->addMediaFromRequest('attachment')->toMediaCollection('templates');
        }
        $approvers = $request->input('approvers') ? $request->input('approvers') : [];
        $docType->approvers()->attach($approvers);

        return redirect()->route('doctypes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentType $doctype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentType $doctype)
    {
        $users=User::where('id','!=',1)->get();
        return view('doctypes.edit',compact('doctype','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentType $doctype)
    {
      //  $user = User::findOrFail($id);
        $doctype->update($request->all());
        $doctype->has_template=$request->has('has_template');
        $doctype->chief_approval=$request->has('chief_approval');
        $doctype->save();
        $approvers = $request->input('approvers') ? $request->input('approvers') : [];
        $doctype->approvers()->sync($approvers);
        if($request->hasFile('attachment')) {
            $doctype->clearMediaCollection('templates');
            $doctype->addMediaFromRequest('attachment')->toMediaCollection('templates');
        }
        return redirect()->route('doctypes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentType $doctype)
    {
//        dd($doctype->id);
        $doctype->delete();
        return redirect()->route('doctypes.index');
    }

    public function download(Media $mediaItem)
    {
        return response()->download($mediaItem->getPath(), $mediaItem->file_name);
    }
    public function media(DocumentType $doctype)
    {
        $mediaItem=$doctype->getFirstMedia('templates');
        return response()->download($mediaItem->getPath(), $mediaItem->file_name);
    }
}
