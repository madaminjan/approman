<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class ProfileController extends Controller
{
    use HasMediaTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
//        $avatar=auth()->user()->getFirstMedia('avatar')->getUrl();
        $user=auth()->user();
        return view('user.profile',compact('user'));
    }

    public function updateAvatar(Request $request)
    {
        $user=Auth::user();
        if($request->hasFile('avatar')) {
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatar');
        }
        return redirect('/profile');
    }
    public function getAvatar()
    {
        $user=Auth::user();
        if($user->getFirstMediaPath('avatar')) {
            return readfile($user->getFirstMediaPath('avatar'));
        }
        return '';
    }
    public function getAvatarThumb()
    {
        $user=Auth::user();
        return $user->getAvatarThumb();
    }
}
