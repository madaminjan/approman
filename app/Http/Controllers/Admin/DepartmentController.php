<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list=Department::parents()->with('departments')->get();
//        $list=Department::withCount('users')->get();
        return view('admin.departments.index',compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locale = \App::getLocale();
        $name=$locale=='ru' ? 'name_ru' : 'name_uz';
        $departments=Department::get()->pluck($name,'id');
        $types=['DIV'=>Lang::get('global.division'),'DEP'=>Lang::get('global.department')];
        $userLists=User::where('id','!=',1)->get();
        $users=array();
        $users[null]='--Нет--';
        foreach ($userLists as $user) {
            $users[$user->id]=$user->shortName();
        }
        return view('admin.departments.create',compact('departments','users','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      /*  $this->validate(request(),[
            'name_ru' => 'required',
            'name_uz' => 'required'
        ]);*/

        $department=Department::create($request->all());

//        dd($request);
        if($request->get('type')=='DIV') $department->parent_id=null;
        $department->save();

        return redirect()->route('admin.department.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        $locale = \App::getLocale();
        $name=$locale=='ru' ? 'name_ru' : 'name_uz';
        $departments=Department::where('id','!=',$department->id)->get()->pluck($name,'id');
        $types=['DIV'=>Lang::get('global.division'),'DEP'=>Lang::get('global.department')];
        $userLists=User::where('id','!=',1)->get();
        $users=array();
        $users[null]='--Нет--';
        foreach ($userLists as $user) {
            $users[$user->id]=$user->shortName();
        }
//        dd($department->id);
        return view('admin.departments.edit',compact('department','departments','users','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $department->update($request->all());
        if($request->get('type')=='DIV') $department->parent_id=null;
        $department->save();

        return redirect()->route('admin.department.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();
        return redirect()->route('admin.department.index');
    }
}
