<?php

namespace App\Http\Controllers\API;

use App\Draft;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function fileSave(Request $request)
    {
        $draft=Draft::find($request['draft']);
        $draft->addMediaFromRequest('file')->toMediaCollection('attachment');
        return response()->json(['result'=>'success']);
    }
}
