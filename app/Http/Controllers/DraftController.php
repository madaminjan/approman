<?php

namespace App\Http\Controllers;

use App\Comment;
use App\DocumentType;
use App\Draft;
use App\Http\Requests\StoreDraftRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DraftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Created by me
        $list=Auth::user()->drafts()->withCount('allComments')->with('approvers')->latest()->paginate(15);
        return view('drafts.index',compact('list'));
    }
    public function approvals()
    {
        $list=Auth::user()->pendingApprovals()->with('approvers')->withCount('allComments')->latest()->paginate(15);
        return view('drafts.index',compact('list'));
    }
    public function archived()
    {
        $list=Auth::user()->archivedApprovals()->with('approvers')->withCount('allComments')->latest()->paginate(15);
        return view('drafts.index',compact('list'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = DocumentType::get()->pluck('name_ru', 'id');
        $types->prepend('--Не выбран--','');
        return view('drafts.create',compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDraftRequest $request)
    {
        $doc=Draft::create($request->all());
        //Save attachment
        if($request->hasFile('attachment')) {
            $doc->addMediaFromRequest('attachment')->toMediaCollection('attachment');
        }
        //Approvers
        $approvers=[];
        $dt=DocumentType::find($doc->document_type_id);
        if($dt) {
            if($dt->chief_approval) {
                $boss=Auth::user()->getBoss();
                if($boss) {
                    $approvers[]=$boss->id;
                }
            }
            $apprs=$dt->approvers;
            if($apprs) {
                foreach ($apprs as $appr) {
                    $approvers[]=$appr->id;
                }
            }
            if(count($approvers)>0) {
                $doc->approvers()->attach($approvers);
                if($dt->seq=='SEQ') {
                    //sequental
                    $doc->approvers()->update(['status'=>'QUEUED']);
                    $doc->approvers()->first()->pivot->update(['status'=>'PENDING']);
                }
            }
        }
        return redirect(route('drafts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function show(Draft $draft)
    {
        return view('drafts.view',compact('draft'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function edit(Draft $draft)
    {
        $types = DocumentType::get()->pluck('name_ru', 'id');
        return view('drafts.edit',compact('draft','types'));
    }

    public function setRead(Draft $draft,$id)
    {
        $draft->approvers->find($id)->pivot->update(['read_at'=>\Carbon\Carbon::now()]);
        $result=array('result'=>'success');
        return response()->json($result);
    }
   /* public function approveDraft(Draft $draft,$id)
    {
        $draft->approvers->find($id)->pivot->update(['status'=>'APPROVED','approved_at'=>\Carbon\Carbon::now()]);
        $result=array('result'=>'success');
        return response()->json($result);
    }*/
    public function approveDraft(Request $request)
    {
        $draft=Draft::find($request['draft']);
        $appId=$request['approver'];
        $draft->approvers->find($appId)->pivot->update(['status'=>'APPROVED','approved_at'=>\Carbon\Carbon::now()]);
        if($draft->type->seq=='SEQ') {
            //Pass to next approver
            $draft->queuedApprovers()->first()->pivot->update(['status'=>'PENDING']);
        }
        if($draft->isAllApproved()) {
            //Set draft approved
            $draft->status='APPROVED';
            $draft->save();
        }
        return redirect('/drafts/'.$draft->id);
    }
    public function rejectDraft(Request $request)
    {
        $draft=Draft::find($request['draft']);
        $appId=$request['approver'];
        $draft->approvers->find($appId)->pivot->update(['status'=>'RETURNED']);

        $comment=new Comment();
        $comment->draft_id=$draft->id;
        $comment->user_id=Auth::user()->id;
        $comment->body=$request['comment'];
        $comment->save();

        return redirect('/drafts/'.$draft->id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(request(),[
            'subject' =>'required',
        ]);
        $draft=Draft::find($request["id"]);
        $draft->subject=$request["subject"];

        //Save attachment
        if($request->hasFile('attachment')) {
            $draft->addMediaFromRequest('attachment')->toMediaCollection('attachment');
            $draft->increment('version');
            //Set approvers status when file changed
            $draft->approvers()->update(['read_at'=>null,'approved_at'=>null]);
            if($draft->type->seq=='SEQ') {
                //sequental
                $draft->approvers()->update(['status'=>'QUEUED']);
                $draft->approvers()->first()->pivot->update(['status'=>'PENDING']);
            }else{
                //parallel
                $draft->approvers()->update(['status'=>'PENDING']);
            }
        }
        $draft->save();
        return redirect(route('drafts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Draft  $draft
     * @return \Illuminate\Http\Response
     */
    public function destroy(Draft $draft)
    {
        $draft->delete();
        return redirect(route('drafts.index'));
    }

    public function typeData(DocumentType $type)
    {
        $me=Auth::user();
        $users=$type->approvers()->get();
        $approvers=array();
        if($type->chief_approval) {
            $boss=$me->getBoss();
            if($boss) {
                $approvers[]=['id'=>$boss->id,'name'=>$boss->shortName(),'post'=>$boss->position->getName()];
            }
        }

        foreach ($users as $user) {
            $approvers[]=['id'=>$user->id,'seq'=>$type->seq,'name'=>$user->shortName(),'post'=>$user->position->getName()];
        }
        $chiefApr=$type->chief_approval;
        $hasTmplate=$type->has_template;
//        $fileUrl=$type->getFirstMediaUrl('templates');

        $fileUrl=$hasTmplate ? route('doctype.media',$type->id) : '';

        $result=array('result'=>'success','approvers'=>$approvers,'cheif'=>$chiefApr,'hastmp'=>$hasTmplate,'url'=>$fileUrl);
        return response()->json($result);
    }
    public function media(Draft $draft)
    {
        $mediaItem=$draft->getFirstMedia('attachment');
        return response()->download($mediaItem->getPath(), $mediaItem->file_name);
    }
}
