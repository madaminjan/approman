<?php
namespace App;

use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\HasApiTokens;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Hash;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
*/
class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    use HasMediaTrait;
    use HasApiTokens;

    protected $fillable = ['name','last_name','first_name','mid_name','phone','department_id','position_id',
        'email', 'password','remember_token','occupation','last_login','ip',];
    
    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar')
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb')
                    ->width(100)
                    ->height(100);
            });
    }
    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }
    public function documentTypes()
    {
        return $this->belongsToMany(DocumentType::class,'document_type_approvers');
    }
    public function isAdmin()
    {
        return $this->hasRole('administrator');
    }

    public function drafts()
    {
        return $this->hasMany(Draft::class);
    }

    public function approvals()
    {
        return $this->belongsToMany(Draft::class,'draft_approvers');
    }
    public function pendingApprovals()
    {
        return $this->belongsToMany(Draft::class,'draft_approvers')->wherePivot('status','PENDING');
    }
    public function archivedApprovals()
    {
        return $this->belongsToMany(Draft::class,'draft_approvers')->wherePivotIn('status',['APPROVED','REJECTED']);
    }
    public function regLoginTimeFormatted() {
        return empty($this->last_login) ? '' : date('d.m.Y H:i',strtotime($this->last_login));
    }

    public function shortName()
    {
        return $this->last_name.' '.mb_substr($this->first_name,0,1).'.'.mb_substr($this->mid_name,0,1);
    }
    public function fullName()
    {
        return $this->last_name.' '.$this->first_name.' '.$this->mid_name;
    }
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function position()
    {
        return $this->belongsTo(Position::class)->withDefault();
    }
    public function roleName($role)
    {
        $locale=\App::getLocale();
        switch ($role) {
            case 'head':
                $name=$locale=='ru' ? 'Руководитель' : 'Рахбар';
                break;
            case 'user':
                $name=$locale=='ru' ? 'Сотрудник' : 'Ходим';
                break;
            default:
                $name='Администратор';
                break;
        }
        return $name;
    }
    public function getBoss()
    {
        return empty($this->department) ? null : $this->department->head;
    }

    public function getAvatarThumb()
    {
        if($this->getFirstMediaPath('avatar','thumb')) {
            return readfile($this->getFirstMediaPath('avatar','thumb'));
        }
        return readfile(public_path().'/img/noava.png');
    }

   /* public function positionName()
    {
        $position=$this->position();
        if(!$position) return '';
        $locale=\App::getLocale();
        return $locale=='ru' ? $position->name_ru : $position->name_uz;
    }*/
}
