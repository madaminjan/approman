<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Carbon;

class Draft extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable=['user_id','subject','document_type_id','status'];
    protected $searchable = ['name'];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->user_id = Auth::id();
        });
    }
    public function registerMediaCollections()
    {
        $this->addMediaCollection('attachment')
            ->singleFile();
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function type()
    {
        return $this->belongsTo(DocumentType::class,'document_type_id');
    }
    public function approvers()
    {
        return $this->belongsToMany(User::class,'draft_approvers')
            ->withPivot('status','read_at','approved_at')->withTimestamps();
    }
    public function queuedApprovers()
    {
        return $this->belongsToMany(User::class,'draft_approvers')->wherePivot('status','QUEUED');
    }
    public function isAllApproved()
    {
        $approvedCount=$this->approvers()->wherePivot('status','APPROVED')->count();
        $total=$this->approvers()->count();
        return $approvedCount==$total;
    }
    public function comments()
    {
        return $this->hasMany(Comment::class)->whereNull('parent_id');
    }
    public function allComments()
    {
        return $this->hasMany(Comment::class);
    }
    public function getRegDateAttribute() {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d.m.Y H:i');
    }
    public function statusname()
    {
        $states=Lang::get('global.statusname');
        return $states[$this->status];
    }
    public function approverStatus($status,$with_label=true)
    {
        $states=Lang::get('global.approver_states');
        if($with_label) {
            $labelColor='';
            switch ($status)
            {
                case 'PENDING':
                    $labelColor='orange';
                    break;
                case 'APPROVED':
                    $labelColor='green';
                    break;
                case 'RETURNED':
                    $labelColor='red';
                    break;
                default:
                    $labelColor='';
            }
            $ret='<span class="ui '.$labelColor.' label">'.$states[$status].'</span>';
        }else{
            $ret=$states[$status];
        }
        return $ret;
    }
    public function getStatusClass() {

        switch ($this->status) {
            case 'PENDING':
                $class="ui teal label";
                break;
            case 'APPROVED':
                $class="ui green label";
                break;
            case 'REJECTED':
                $class="ui red label";
                break;
            default:
                $class="ui label";
                break;
        }
        return $class;
    }
}
