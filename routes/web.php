<?php
Route::get('/', 'HomeController@index')->name('home');
Route::get('language/{lang}', 'HomeController@language');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
Route::post('login', 'Auth\LoginController@login')->name('auth.login');
Route::post('logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::resource('drafts', 'DraftController');
Route::resource('comments', 'CommentController');
Route::get('pending','DraftController@approvals')->name('drafts.pending');
Route::get('archived','DraftController@archived')->name('drafts.archived');
Route::get('approvals/setread/{draft}/{id}','DraftController@setRead')->name('drafts.setread');
Route::post('drafts/approve','DraftController@approveDraft')->name('drafts.approve');
Route::post('drafts/reject','DraftController@rejectDraft')->name('drafts.reject');
Route::resource('doctypes', 'DocumentTypeController');

Route::get('/doctype/media/{doctype}','DocumentTypeController@media')->name('doctype.media');

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
Route::post('password/email'    , 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::get('/template/download/{mediaItem}','DocumentTypeController@download')->name('doctypes.download');
Route::get('/drafts/typedata/{type}','DraftController@typeData')->name('drafts.typedata');
Route::get('/drafts/media/{draft}','DraftController@media')->name('drafts.media');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', ['uses' => 'Admin\PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::resource('department', 'Admin\DepartmentController');
    Route::resource('position', 'Admin\PositionController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
});

Route::get('/profile','ProfileController@profile')->name('profile');
Route::post('/profile/avatar','ProfileController@updateAvatar')->name('avatar.update');
Route::get('/profile/avatar','ProfileController@getAvatar')->name('avatar.get');
Route::get('/profile/avatar-thumb','ProfileController@getAvatarThumb')->name('avatar.thumb');
Route::get('/avatar-thumb/{user}','CommonController@getAvatarThumb')->name('common.avatar.thumb');
